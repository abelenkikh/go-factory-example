package main

import (
	"errors"
	"fmt"
)

type Hello struct {
	val string
}

func (a Hello) Result() string {
	return "hello"
}

type World struct {
	val string
}

func (a World) Result() string {
	return "world"
}

type ConcreteCreater struct {
}

type Creater interface {
	CreateProduct(action string) (Producter, error)
}

type Producter interface {
	Result() string
}

func NewCreater() Creater {
	return &ConcreteCreater{}
}

func (p *ConcreteCreater) CreateProduct(action string) (Producter, error) {
	commits := map[string]Producter{
		"WORLD": &World{},
		"HELLO": &Hello{},
	}

	if product, ok := commits[action]; ok {
		return product, nil
	}

	return nil, errors.New("unknown action")
}

func main() {
	factory := NewCreater()

	product, _ := factory.CreateProduct("HELLO")
	if product != nil {
		fmt.Printf("%s", product.Result())
	}

	product2, _ := factory.CreateProduct("WORLD")
	if product2 != nil {
		fmt.Printf("%s", product2.Result())
	}
}
