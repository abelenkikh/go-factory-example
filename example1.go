package main

import (
	"fmt"
)

type ProductInterface interface {
	Result() string
}

type Hello struct {
	val string
}

func (a Hello) Result() string {
	return "hello"
}

type World struct {
	val string
}

func (a World) Result() string {
	return "world"
}

var factory = map[string]ProductInterface{
	"WORLD": &World{},
	"HELLO": &Hello{},
}

func main() {
	fmt.Printf("%s", factory["HELLO"].Result())
	fmt.Printf("%s", factory["WORLD"].Result())
}
